#!/bin/bash

set -euo pipefail

# This script is the ENTRYPOINT defined in the Dockerfile, and it's purpose is to prepare the environment for running the process in the container to use in the app.

report() {
  printf "[entrypoint.sh] %s\n" "$1" >&2
}

/app/bin/run_jmeter-ec2

report "exec $*"
exec $@
