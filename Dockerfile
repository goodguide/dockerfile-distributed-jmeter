FROM quay.io/goodguide/oracle-java:ubuntu-18.04-1
# FROM quay.io/goodguide/oracle-java:ubuntu-15.10-java-8.66-0

MAINTAINER Goodguide "docker@goodguide.com"

RUN apt-get update \
 && apt-get install \
      autoconf \
      build-essential \
      cron \
      git \
      #Required by Ruby:
      libssl-dev \
      libreadline-dev \
      zlib1g-dev \
      # Required by jmeter-ec2:
      awscli \
      bc \
      openssh-client

ENV RUBY_VERSION="2.5.1" RUBY_PREFIX=/usr/local

# Disable default RDoc/ri generation when installing gems
RUN echo 'gem: --no-rdoc --no-ri' >> /usr/local/etc/gemrc \

# Use ruby-build to install Ruby
 && git clone https://github.com/sstephenson/ruby-build.git /tmp/ruby-build \

# Install ruby via ruby-build
 && /tmp/ruby-build/bin/ruby-build -v "$RUBY_VERSION" "$RUBY_PREFIX" \

 && rm -rf /tmp/ruby-build \

 # update rubygems
 && gem update --system \
 && gem install bundler --force \
 && gem install rake --force \
 && gem install concurrent-ruby

# for dependant images, again update rubygems then install bundler upon building off this image to ensure everything's up-to-date
ONBUILD RUN gem update --system \
 && gem install bundler --force

WORKDIR /app

ENV PATH="/app/bin:$PATH"

COPY ./bin/ /app/bin/
COPY ./docker/ /app/docker/

COPY ./jmeter-project/ /app/jmeter-project/

RUN mkdir -p /app/tmp

RUN cd /usr/local && git clone https://github.com/oliverlloyd/jmeter-ec2.git
COPY ./usr/local/jmeter-ec2/jmeter-ec2.properties.template /usr/local/jmeter-ec2/jmeter-ec2.properties.template

ENTRYPOINT ["/sbin/tini", "-vvg", "--", "/app/docker/entrypoint.sh"]

STOPSIGNAL SIGINT

# CMD ["/bin/bash"]
