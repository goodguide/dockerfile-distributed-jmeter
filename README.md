# Distributed JMeter

Runs [jmeter-ec2][jmeter_ec2] inside a docker container.

[Apache JMeter][jmeter] is Java application which tests and measures web application performance. Jemter-ec2 launches several EC2 instances, each with a copy of your JMeter test suite, and returns the results. As your credit card is the limit on instances launched, this a good way to test scaling and load balancing for web applications. This is more useful if used in conjunction with more user friendly monitoring software like [New Relic][new relic].

## Security Notice!

The docker images created by this application contain AWS IAM credentials in the ENV variables. Secure this docker image as would any other sensitive credentials!

## Setup

This assumes you are using macOS or something *NIX-like.

### AWS
• Have an AWS IAM account with access to create VPCs, Subnets, EC2, etc.
• Make a AWS Key Pair and save PEM file to ```./AWS_PEM/```.

### Automated AWS Infrastructure using Terraform

[Terraform][terraform] is an Infrastructure as Code tool and we will be using it to setup a VPC and networking for our jmeter-ec2 instances.
]
#### AWS S3 Bucket to Store Infrastructure State

Go to the AWS Console and create an S3 bucket, named ```com.goodguide.terraform.jmeter-ec2```.  If you don't like that name, you may change it in the Terraform [main.tf][main-tf] file. Do to reasons, this can't be set dynamicaly. This bucket will hold the state of the AWS infrastructure resources you are managing with terraform.

#### Direnv

[Direnv][Direnv] sets some ENV variables in the current shell while you work within this project and unsets them if you navigate out of it. Edit the ```.envrc.example``` file to provide the information terraform needs to run.

```bash
brew install direnv #macOS
apt-get install direnv # Ubuntu, etc...
cp .envrc.example .envrc
vi .envrc # edit the file to add your information
  ...
direnv allow
```
#### Terraform

Terraform is wrapped in a Docker container to insure things go smoothly. Install [Docker CE for Mac][Docker for Mac] if you havent already.

```bash
cd ./terraform
tf init # tf is the wrapper script; there will be a lot of docker output first run.
tf plan
tf apply
```
If terraform exited successfully, you will have built a VPC, subnets, an Internet gateway, and security group rules on AWS.  As the state of these resources is shared in the S3 bucket, they will be available to other users in your group too. If there is nothing for terraform to build, one of your compatriots has probably already run terraform.

Go to the AWS console and note down one of the subnet ids and the security group id.  You will add them to the docker-compose.override.yml file.

#### JMeter and Java

Jmeter-ec2 uses [JMeter 2.13][jmeter_download] which we run on [Java 8][java_download].  Install both locally.

### Configuring JMeter

Each project to be tested needs to have it's own branch:

```bash
git checkout master
git pull
git checkout -b project-<name> # i.e. project-ulpurview
# Change the directory and jmx file name to match (important!):
mv jmeter-project/jmx/jmeter-project.jmx jmeter-project/jmx/project-<name>.jmx
mv jmeter-project project-<name>
```

Open the ```project-<name>/jmx/project-<name>.jmx``` file in the jmeter GUI and set up your test.  That's outside the scope of this documentation, the the [offical docs][jmeter_docs].

## Usage

Configure, build, and run the docker container:

```bash
cp docker-compose.override.example.yml docker-compose.override.yml
vi docker-compose.override.yml # Fill in the ENV variables.
  ...
docker-compose build
docker-compose run app
```

[Direnv]: https://direnv.net
[Docker for Mac]: https://store.docker.com/editions/community/docker-ce-desktop-mac
[java_download]: https://www.java.com/en/
[jmeter]: https://jmeter.apache.org
[jmeter_docs]: https://jmeter.apache.org/usermanual/get-started.html
[jmeter_download]: https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-2.13.zip
[jmeter_ec2]: https://github.com/oliverlloyd/jmeter-ec2
[main-tf]: https://gitlab.com/goodguide/dockerfile-distributed-jmeter/blob/master/main.tf#L4
[new relic]: https://newrelic.com
[terraform]: https://www.terraform.io

