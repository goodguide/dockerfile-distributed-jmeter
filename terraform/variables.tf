variable "aws_s3_bucket_name" {
  default = "com.goodguide.terraform.jmeter-ec2"
}

variable "name" {
  default = "jmeter-ec2"
}

variable "region" {
  default = "us-east-1"
}

variable "subnet_bitsize" {
  default = 8
}

variable "cidr" {
  default = "10.0.0.0/16"
}
